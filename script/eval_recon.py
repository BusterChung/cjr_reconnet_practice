"""
yuv_I420_video 를 frame 별로 읽어서 복원해주는 스크립트.
"""

from pydooly import *
from models import *
from os.path import join
import torch
import tqdm

"""
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
y 에 대해 학습시킨 것을 y,u,v 모두에 eval 할 때 사용하는 코드
"""
def video_recon(video_dir, pretrained_net_dir, video_HW, total_frame):
    """
    y에 학습시킨 모델을 u,v에 동일하게 적용시킨 것.
    """
    # 딥러닝 모델 초기화
    device = torch.device('cuda:0')
    input_channel = 1  # 흑백이미지일 경우 1, 컬러 이미지일 경우 3
    netG = Generator_one2many_RDB(input_channel).to(device)
    if os.path.isfile(pretrained_net_dir):  # 저장된 weight 들 불러오기.
        checkpoint = torch.load(pretrained_net_dir)
        netG.load_state_dict(checkpoint['state_dict_G'])
    else:
        print("===> no checkpoint found at '{}'".format(pretrained_net_dir))

    # opencv 의 numpy.ndarray 에서 torch.FloatTensor 로 바꿔주는 함수
    transform = Compose([
        Color0_255to1_1(),
        ToTensor()
    ])
    # torch.FloatTensor 에서 opencv 의 numpy.ndarray 로 바꿔주는 함수
    inverse_transform = Compose([
        ToImage(),
        Color1_1to0_255(),
    ])

    def reconstructor(img):
        img = transform(img)
        img = img.view(1, -1, img.shape[1], img.shape[2])

        with torch.no_grad():
            netG.eval()
            prediction = netG(img.to(device))

        prediction = inverse_transform(prediction)
        return prediction

    # YUV reader init.
    cap = VideoCaptureYUV(video_dir, video_HW)
    frame_num = 1

    height, width = video_HW
    frame = 0
    HxW = width * height
    HxW_4 = int(HxW *0.25)
    new_video = np.zeros((int(HxW * 1.5 * total_frame)))

    for _ in tqdm.tqdm(range(total_frame)):
        ret, y, u, v = cap.read_yuv()
        # cv2.imshow('frame', y)
        # cv2.waitKey(10)
        if ret:
            #print('frame num :', frame_num)
            frame_num += 1

            y = reconstructor(y.astype(np.float32))
            u = reconstructor(u.astype(np.float32))
            v = reconstructor(v.astype(np.float32))

            y = y.flatten()
            u = u.flatten()
            v = v.flatten()

            new_video[frame:frame + HxW] = y[:]
            new_video[frame + HxW:frame + HxW + HxW_4] = u[:]
            new_video[frame + HxW + HxW_4:frame + HxW + HxW_4 * 2] = v[:]
            frame += int(HxW * 1.5)

    return new_video


def video_folder_recon(pretrained_nets_folder_dir, artifacted_videos_folder_dir, recon_videos_folder_dir):
    """
    y에 학습시킨 모델을 u,v에 동일하게 적용시킨 것.
    """
    video_dirs = [join(artifacted_videos_folder_dir, x) for x in listdir(artifacted_videos_folder_dir)]
    for video_dir in video_dirs:
        print('==>> 복원 할 비디오의 경로 :', video_dir)

        # 비디오를 복원할 pkl 파일의 경로 생성
        QP = video_dir.split('QP')[1].split('_')[0]  # 복원할 비디오의 QP.
        dir_saved_netG = pretrained_nets_folder_dir + '\\' + QP + '.pkl'
        print('==>> 비디오를 복원할 네트워크 :', dir_saved_netG)

        # 복원한 비디오를 저장할 경로 생성
        recon_video_dir = recon_videos_folder_dir
        dir_video_ = video_dir.split('\\')[-3:-1]
        for s in dir_video_:
            recon_video_dir += ('\\' + s)
        recon_video_dir = make_dirs(recon_video_dir)
        # 복원된 비디오의 새 이름 생성 (_recon 을 붙여준다)
        video_name = os.path.basename(video_dir)
        video_name = os.path.splitext(video_name)[0]
        recon_video_name = video_name + '_recon.yuv'
        # 경로와 이름을 합해준다.
        recon_video_dir = recon_video_dir + '\\' + recon_video_name
        print('==>> 복원 될 비디오의 경로 :', recon_video_dir)

        # 비디오의 height, width
        size = video_name.split('_')[2].split('x')
        size = (int(size[1]), int(size[0]))
        print('==>> H, W :', size)

        # 비디오의 total frame 길이
        total_frame = int(video_name.split('frm')[0].split('_')[-1])
        print('==>> 비디오의 길이 :', total_frame)

        # 새로 생성될 파일의 경로 생성
        recon_video = video_recon(video_dir, dir_saved_netG, size, total_frame=total_frame)

        # 복원된 비디오를 위에서 생성한 경로에 저장한다.
        with open(recon_video_dir, "wb") as f:
            f.write(recon_video.astype('uint8').tobytes())


def ctc_of_hevc_recon(pretrained_nets_folder_dir, ctc_dir, recon_videos_folder_dir):
    """
    y에 학습시킨 모델을 u,v에 동일하게 적용시킨 것.
    """
    folder1s = [join(ctc_dir, x) for x in listdir(ctc_dir)]
    for folder1 in folder1s[1:]:
        folder2s = [join(folder1, x) for x in listdir(folder1)]
        for folder2 in folder2s:
            print(folder2, '-------------------------------------------------------------------------------------')
            video_folder_recon(pretrained_nets_folder_dir, folder2, recon_videos_folder_dir)


"""
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
y, u, v 각각에 학습시킨 것을 각각 eval 하는 코드
"""
def loadnet(netG, pretrained_net_dir):
    if os.path.isfile(pretrained_net_dir):  # 저장된 weight 들 불러오기.
        checkpoint = torch.load(pretrained_net_dir)
        netG.load_state_dict(checkpoint['state_dict_G'])
    else:
        print("===> no checkpoint found at '{}'".format(pretrained_net_dir))


def video_recon2(video_dir, pretrained_net_dir, video_HW, total_frame):
    """
    y, u, v 각각 따로 적용한 것.
    """
    # 딥러닝 모델 초기화
    device = torch.device('cuda:0')
    input_channel = 1  # 흑백이미지일 경우 1, 컬러 이미지일 경우 3

    netG_y = Generator_one2many_RDB(input_channel).to(device)
    netG_u = Generator_one2many_RDB(input_channel).to(device)
    netG_v = Generator_one2many_RDB(input_channel).to(device)

    loadnet(netG_y, pretrained_net_dir[0])
    loadnet(netG_u, pretrained_net_dir[1])
    loadnet(netG_v, pretrained_net_dir[2])

    # opencv 의 numpy.ndarray 에서 torch.FloatTensor 로 바꿔주는 함수
    transform = Compose([
        Color0_255to1_1(),
        ToTensor()
    ])
    # torch.FloatTensor 에서 opencv 의 numpy.ndarray 로 바꿔주는 함수
    inverse_transform = Compose([
        ToImage(),
        Color1_1to0_255(),
    ])

    def reconstructor(img, netG):
        img = transform(img)
        img = img.view(1, -1, img.shape[1], img.shape[2])

        with torch.no_grad():
            netG.eval()
            prediction = netG(img.to(device))

        prediction = inverse_transform(prediction)
        return prediction

    # YUV reader init.
    cap = VideoCaptureYUV(video_dir, video_HW)
    frame_num = 1

    height, width = video_HW
    frame = 0
    HxW = width * height
    HxW_4 = int(HxW *0.25)
    new_video = np.zeros((int(HxW * 1.5 * total_frame)))

    for _ in tqdm.tqdm(range(total_frame)):
        ret, y, u, v = cap.read_yuv()
        # cv2.imshow('frame', y)
        # cv2.waitKey(10)
        if ret:
            #print('frame num :', frame_num)
            frame_num += 1

            y = reconstructor(y.astype(np.float32), netG_y)
            u = reconstructor(u.astype(np.float32), netG_u)
            v = reconstructor(v.astype(np.float32), netG_v)

            y = y.flatten()
            u = u.flatten()
            v = v.flatten()

            new_video[frame:frame + HxW] = y[:]
            new_video[frame + HxW:frame + HxW + HxW_4] = u[:]
            new_video[frame + HxW + HxW_4:frame + HxW + HxW_4 * 2] = v[:]
            frame += int(HxW * 1.5)

    return new_video


def video_folder_recon2(pretrained_nets_folder_dir, artifacted_videos_folder_dir, recon_videos_folder_dir):
    """
    y, u, v 각각 따로 적용한 것.
    """
    video_dirs = [join(artifacted_videos_folder_dir, x) for x in listdir(artifacted_videos_folder_dir)]
    for video_dir in video_dirs:
        print('==>> 복원 할 비디오의 경로 :', video_dir)

        # 비디오를 복원할 pkl 파일의 경로 생성
        QP = video_dir.split('QP')[1].split('_')[0]  # 복원할 비디오의 QP.
        dir_saved_netG_y = pretrained_nets_folder_dir + '\\' + QP + '.pkl'
        dir_saved_netG_u = pretrained_nets_folder_dir + '\\' + QP + 'u.pkl'
        dir_saved_netG_v = pretrained_nets_folder_dir + '\\' + QP + 'v.pkl'
        dir_saved_netG = (dir_saved_netG_y, dir_saved_netG_u, dir_saved_netG_v)
        print('==>> 비디오를 복원할 네트워크 :', dir_saved_netG)

        # 복원한 비디오를 저장할 경로 생성
        recon_video_dir = recon_videos_folder_dir
        dir_video_ = video_dir.split('\\')[-3:-1]
        for s in dir_video_:
            recon_video_dir += ('\\' + s)
        recon_video_dir = make_dirs(recon_video_dir)
        # 복원된 비디오의 새 이름 생성 (_recon 을 붙여준다)
        video_name = os.path.basename(video_dir)
        video_name = os.path.splitext(video_name)[0]
        recon_video_name = video_name + '_recon.yuv'
        # 경로와 이름을 합해준다.
        recon_video_dir = recon_video_dir + '\\' + recon_video_name
        print('==>> 복원 될 비디오의 경로 :', recon_video_dir)

        # 비디오의 height, width
        size = video_name.split('_')[2].split('x')
        size = (int(size[1]), int(size[0]))
        print('==>> H, W :', size)

        # 비디오의 total frame 길이
        total_frame = int(video_name.split('frm')[0].split('_')[-1])
        print('==>> 비디오의 길이 :', total_frame)

        # 새로 생성될 파일의 경로 생성
        recon_video = video_recon2(video_dir, dir_saved_netG, size, total_frame=total_frame)

        # 복원된 비디오를 위에서 생성한 경로에 저장한다.
        with open(recon_video_dir, "wb") as f:
            f.write(recon_video.astype('uint8').tobytes())


def ctc_of_hevc_recon2(pretrained_nets_folder_dir, ctc_dir, recon_videos_folder_dir):
    """
    y, u, v 각각 따로 적용한 것.
    """
    folder1s = [join(ctc_dir, x) for x in listdir(ctc_dir)]
    for folder1 in folder1s[1:]:
        folder2s = [join(folder1, x) for x in listdir(folder1)]
        for folder2 in folder2s:
            print(folder2, '-------------------------------------------------------------------------------------')
            video_folder_recon2(pretrained_nets_folder_dir, folder2, recon_videos_folder_dir)


if __name__ == '__main__':
    # 미리 학습된 네트워크들(22, 27, 32, 37) 이 있는 폴더의 dir.
    pretrained_nets_folder_dir = r'C:\kdw\reconNet_1.1\pretrained_nets'

    # ctc_of_hevc 가 있는 폴더의 dir.
    ctc_dir = r'D:\Data\ctc_of_hevc'

    # 복원될 비디오들이 저장될 폴더의 dir.
    recon_videos_folder_dir = r'D:\kdw\reconNet_1.1\eval2'

    # 복원!
    ctc_of_hevc_recon2(pretrained_nets_folder_dir, ctc_dir, recon_videos_folder_dir)

    """
    # 비디오 하나만 복원하고 싶을때 예제 코드::
    video_dir = r'D:\Data\ctc_of_hevc\[1]IntraOnly\[3]ClassD\S17_RaceHorses_416x240_P420_30Hz_300frm_QP37_IntraOnly.yuv'
    pretrained_net_dir = r'C:\kdw\reconNet_1.1\pretrained_nets\37.pkl'
    video_HW = (240, 416)
    recon_video = video_recon(video_dir, pretrained_net_dir, video_HW, total_frame=300)
    with open('S17_RaceHorses_416x240_P420_30Hz_QP37_IntraOnly_recon.yuv', "wb") as f:
        f.write(recon_video.astype('uint8').tobytes())
    """
