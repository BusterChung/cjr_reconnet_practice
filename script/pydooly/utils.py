import os
import shutil
import cv2
import numpy as np
import torch
import csv


def save_checkpoint(state, is_best, filename='checkpoint.pkl'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, os.path.dirname(filename) + '\\checkpoint_best.pkl')


class LogCSV(object):
    def __init__(self, log_dir, header, epoch):
        """
        :param log_dir: log(csv 파일) 가 저장될 dir
        :param header:  log 찍기 전에 header 을 먼저 write 해준다.
        """
        self.log_dir = log_dir
        self.header = header

        # epoch 이 1 일 경우(학습이 막 시작했을 경우) 에만 header 을 찍어준다.
        if epoch == 1:
            with open(self.log_dir, "a") as output:
                writer = csv.writer(output, lineterminator='\n')
                writer.writerow(header)

    def __call__(self, log):
        """
        :param log: header 의 각 항목에 해당하는 값들의 list
        """
        with open(self.log_dir, "a") as output:
            writer = csv.writer(output, lineterminator='\n')
            writer.writerow(log)


class LogCSV2(object):
    def __init__(self, log_dir, header):
        """
        :param log_dir: log(csv 파일) 가 저장될 dir
        :param header:  log 찍기 전에 header 을 먼저 write 해준다.
        """
        self.log_dir = log_dir
        self.header = header

        with open(self.log_dir, "a") as output:
            writer = csv.writer(output, lineterminator='\n')
            writer.writerow(header)

    def __call__(self, log):
        """
        :param log: header 의 각 항목에 해당하는 값들의 list
        """
        with open(self.log_dir, "a") as output:
            writer = csv.writer(output, lineterminator='\n')
            writer.writerow(log)

def args2memo(args, dir):
    f = open(dir, 'w')
    for arg in vars(args):
        data = str(arg) + ' = ' + str(getattr(args, arg)) + '\n'
        f.write(data)
    f.close()

def make_dirs(path):
    """
    경로(폴더) 가 있음을 확인하고 없으면 새로 생성한다.
    :param path: 확인할 경로
    :return: path
    """
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def adjust_learning_rate(init_lr, optimizer, epoch, n=70):
    """Sets the learning rate to the initial learning rate decayed by 10 every n epochs"""
    init_lr = init_lr * (0.1 ** (epoch // n))
    print('learning rate : ', init_lr)
    for param_group in optimizer.param_groups:
        param_group['lr'] = init_lr


def seconds_to_hours(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%02d" % (h, m, s)


def imshow(img):
    """
    이미지를 저장하지 않고 바로바로 보고싶을 때 사용한다.
    :param img: <class 'numpy.ndarray'> 의 이미지
    """
    cv2.imshow('image', img.astype(np.uint8))
    cv2.waitKey(10)
    # cv2.destroyAllWindows()


