"""
https://stackoverflow.com/questions/2231518/how-to-read-a-frame-from-yuv-file-in-opencv?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
https://en.wikipedia.org/wiki/YUV
"""
import cv2
import numpy as np


class VideoCaptureYUV:
    def __init__(self, filename, size):
        self.height, self.width = size
        self.img_len = self.width * self.height
        self.frame_len = self.img_len * 3 / 2

        self.f = open(filename, 'rb')
        self.shape = (int(self.height*1.5), self.width)

    def read_raw(self):
        try:
            raw = self.f.read(int(self.frame_len))
            yuv_byte = np.frombuffer(raw, dtype=np.uint8)

        except Exception as e:
            print(str(e))
            return False, None
        return True, yuv_byte

    def read_y(self):
        ret, yuv_byte = self.read_raw()
        y = yuv_byte[0:self.img_len]
        y = y.reshape(self.height, self.width)
        return ret, y

    def read_u(self):
        ret, yuv_byte = self.read_raw()
        u = yuv_byte[self.img_len:self.img_len + int(self.img_len / 4)]
        u = u.reshape(int(self.height*0.5), int(self.width*0.5))
        return ret, u

    def read_v(self):
        ret, yuv_byte = self.read_raw()
        v = yuv_byte[self.img_len + int(self.img_len / 4):]
        v = v.reshape(int(self.height * 0.5), int(self.width * 0.5))
        return ret, v

    def read_yuv(self):
        ret, yuv_byte = self.read_raw()

        if len(yuv_byte) == 0:
            return False, yuv_byte, yuv_byte, yuv_byte

        y = yuv_byte[0:self.img_len]
        y = y.reshape(self.height, self.width)
        u = yuv_byte[self.img_len:self.img_len + int(self.img_len / 4)]
        u = u.reshape(int(self.height * 0.5), int(self.width * 0.5))
        v = yuv_byte[self.img_len + int(self.img_len / 4):]
        v = v.reshape(int(self.height * 0.5), int(self.width * 0.5))

        return True, y, u, v

    def read_yuv420(self):
        ret, yuv_byte = self.read_raw()
        yuv420 = yuv_byte.reshape(self.shape)
        return ret, yuv420

    def read_bgr(self):
        ret, yuv = self.read_yuv420()

        if not ret:
            return ret, yuv
        bgr = cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR_I420)
        return ret, bgr
        

if __name__ == "__main__":
    
    filename = "S14_BasketballPass_416x240_P420_50Hz_QP22_IntraOnly.yuv"
    size = (240, 416)
    cap = VideoCaptureYUV(filename, size)

    while 1:
        ret, frame = cap.read_bgr()
        if ret:
            cv2.imshow('frame', frame)
            cv2.waitKey(30)
        else:
            break