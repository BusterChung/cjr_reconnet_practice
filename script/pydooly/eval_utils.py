from math import log10
import torch
import numpy as np
import tqdm
from os.path import join
from os import listdir
from pydooly.YUV_I420_video_reader import VideoCaptureYUV
from pydooly.dataIO import load_img
from pydooly.utils import LogCSV2


def get_psnr(img1, img2, min_value=0, max_value=255):
    """
    psnr 을 계산해준다.
    이미지가 [0., 255] 이면 min_value=0, max_valu=255 로 해주고,
    이미지가 [-1,1]의 범위에 있으면 min_value=-1, max_valu=1 로 설정 해준다.
    """
    if type(img1) == torch.Tensor:
        mse = torch.mean((img1 - img2) ** 2)
    else:
        mse = np.mean((img1 - img2) ** 2)
    if mse == 0:
        return 100
    PIXEL_MAX = max_value - min_value
    return 10 * log10((PIXEL_MAX ** 2) / mse)


def get_psnr_for_folder(imgs_a_dir, imgs_b_dir, min_value=0, max_value=255):
    """
    폴더 단위로 이미지를 비교하여 psnr 을 구할 때 사용.
    비교하려는 두 폴더의 주소값을 각각 넣어주면 각 폴더에 있는 이미지끼리 psnr 을 구하여 평균을 내준다.
    """
    imgs_a_name = [join(imgs_a_dir, x) for x in listdir(imgs_a_dir)]
    imgs_b_name = [join(imgs_b_dir, x) for x in listdir(imgs_b_dir)]
    psnr_sum = 0
    for a_name, b_name in tqdm.tqdm(zip(imgs_a_name, imgs_b_name)):
        A = load_img(a_name)
        B = load_img(b_name)
        p = get_psnr(A, B, min_value, max_value)
        psnr_sum += p
    psnr_mean = psnr_sum / len(imgs_a_name)
    return psnr_mean


def get_psnr_for_video(video_a_dir, video_b_dir, size, min_value=0, max_value=255, log_dir=None):
    """
    두개의 yuv420 비디오를 비교하여 psnr 을 구할 때 사용.
    비교하려는 두 비디오의 주소값을 각각 넣어주면 frame 별 psnr 을 구하여 평균을 내준다.
    """

    psnr_y_sum = 0
    psnr_u_sum = 0
    psnr_v_sum = 0

    # YUV reader init.
    cap1 = VideoCaptureYUV(video_a_dir, size)
    cap2 = VideoCaptureYUV(video_b_dir, size)
    frame_count = 0

    if log_dir is not None:
        logging = LogCSV2(log_dir=log_dir, header=['frame', 'y', 'u', 'v'])

    while 1:
        ret1, y1, u1, v1 = cap1.read_yuv()
        ret2, y2, u2, v2 = cap2.read_yuv()

        # cv2.imshow('frame', y)
        # cv2.waitKey(10)
        if ret1 and ret2:
            frame_count += 1
            # print('frame num :', frame_count)
            # float 형으로 바꿔주었다.
            psnr_y = get_psnr(y1.astype(np.float32), y2.astype(np.float32), min_value, max_value)
            psnr_u = get_psnr(u1.astype(np.float32), u2.astype(np.float32), min_value, max_value)
            psnr_v = get_psnr(v1.astype(np.float32), v2.astype(np.float32), min_value, max_value)

            if log_dir is not None:
                logging([frame_count, psnr_y, psnr_u, psnr_v])

            psnr_y_sum += psnr_y
            psnr_u_sum += psnr_u
            psnr_v_sum += psnr_v
        else:
            #print('End')
            break
    psnr_y = psnr_y_sum/frame_count
    psnr_u = psnr_u_sum / frame_count
    psnr_v = psnr_v_sum / frame_count

    return psnr_y, psnr_u, psnr_v


class TorchPaddingForOdd(object):
    """
    OneToMany 의 경우 Down-Sampling 하는 층이 있다. 이때 1/2 크기로 Down-Sampling 하는데
    이미지 사이즈가 홀수이면 2로 나눌 수 없기 때문에 일시적으로 padding 을 하여 짝수로 만들어 준 후 모델을 통과시키고,
    마지막으로 unpadding 을 하여 원래 이미지 크기로 만들어준다.
    """
    def __init__(self):
        self.is_height_even = True
        self.is_width_even = True

    def padding(self, img):
        # 홀수면 패딩을 체워주는 것을 해주자
        if img.shape[2] % 2 != 0:
            self.is_height_even = False
            img_ = torch.zeros(img.shape[0], img.shape[1], img.shape[2] + 1, img.shape[3])
            img_[:img.shape[0], :img.shape[1], :img.shape[2], :img.shape[3]] = img
            img_[:img.shape[0], :img.shape[1], img.shape[2], :img.shape[3]] = img_[:img.shape[0], :img.shape[1],
                                                                              img.shape[2] - 1, :img.shape[3]]
            img = img_
        if img.shape[3] % 2 != 0:
            self.is_width_even = False
            img_ = torch.zeros(img.shape[0], img.shape[1], img.shape[2], img.shape[3] + 1)
            img_[:img.shape[0], :img.shape[1], :img.shape[2], :img.shape[3]] = img
            img_[:img.shape[0], :img.shape[1], :img.shape[2], img.shape[3]] = img_[:img.shape[0], :img.shape[1],
                                                                              :img.shape[2], img.shape[3] - 1]
            img = img_
        return img

    def unpadding(self, img):
        # 홀수였으면 패딩을 제거하는 것을 해주자
        if not self.is_height_even:
            img.data = img.data[:img.shape[0], :img.shape[1], :img.shape[2] - 1, :img.shape[3]]
        if not self.is_width_even:
            img.data = img.data[:img.shape[0], :img.shape[1], :img.shape[2], :img.shape[3] - 1]
        return img
