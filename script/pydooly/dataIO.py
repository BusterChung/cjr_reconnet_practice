import cv2
import os
import numpy as np


def load_img(filepath):
    """
    :param filepath: 처리하려는 이미지의 dir
    :return: 1 채널 혹은 3채널의 <class 'numpy.ndarray'> 이미지

    예시)
    img = load_grayscale(filepath)
    또는
    img = load_YUV_I420(filepath)
    등..
    """
    img = load_V_From_YUV_I420(filepath)

    # default tensor type is (torch.float32), 때문에 float32 로 형변환을 해준다.
    img = img.astype(np.float32)
    return img


def load_BGR(filepath):
    img_BGR = cv2.imread(filepath, cv2.IMREAD_COLOR)
    return img_BGR


def load_grayscale(filepath):
    img_grayscale = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    return img_grayscale


def load_Y_From_YUV_I420(filepath):
    """
    특히 coco image data set 을 yuv 비디오 형식으로 변환시킨 후 읽기위해 사용되는 함수.

    참고 싸이트 :
    http://crynut84.tistory.com/56
    https://en.wikipedia.org/wiki/YUV
    http://blog.dasomoli.org/265/
    https://picamera.readthedocs.io/en/release-1.10/recipes2.html#unencoded-image-capture-yuv-format
    https://raspberrypi.stackexchange.com/questions/28033/reading-frames-of-uncompressed-yuv-video-file?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

    :param filepath: YUV_I420 포맷 영상의 dir
    :return: yuvi420 비디오의 첫번째 프래임의 YUV 채널 중 Y 채널만 반환

    >>> y = load_YUV_I420('0002018_320x480_P420_8b.yuv')
    >>> cv2.imshow('image', y)
    >>> cv2.waitKey(0)
    """
    # yuv420 포맷에서 중 y 만 읽는다.
    img_name = os.path.basename(filepath)
    img_name = os.path.splitext(img_name)[0]
    w, h = img_name.split('_')[1].split('x')
    w, h = int(w), int(h)
    frame_len = w * h
    f = open(filepath, 'rb')
    try:
        raw = f.read(int(frame_len))
        y = np.frombuffer(raw, dtype=np.uint8)
        # cv2 grayscale image shape is height x width
        img_y = y.reshape(h, w)
    except Exception as e:
        print(str(e))
        return None
    return img_y


def load_U_From_YUV_I420(filepath):
    """
    특히 coco image data set 을 yuv 비디오 형식으로 변환시킨 후 읽기위해 사용되는 함수.

    참고 싸이트 :
    http://crynut84.tistory.com/56
    https://en.wikipedia.org/wiki/YUV
    http://blog.dasomoli.org/265/
    https://picamera.readthedocs.io/en/release-1.10/recipes2.html#unencoded-image-capture-yuv-format
    https://raspberrypi.stackexchange.com/questions/28033/reading-frames-of-uncompressed-yuv-video-file?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

    :param filepath: YUV_I420 포맷 영상의 dir
    :return: yuvi420 비디오의 첫번째 프래임의 YUV 채널 중 Y 채널만 반환

    >>> y = load_YUV_I420('0002018_320x480_P420_8b.yuv')
    >>> cv2.imshow('image', y)
    >>> cv2.waitKey(0)
    """
    # yuv420 포맷에서 중 u 만 읽는다.
    img_name = os.path.basename(filepath)
    img_name = os.path.splitext(img_name)[0]
    w, h = img_name.split('_')[1].split('x')
    w, h = int(float(w)/2), int(float(h)/2)
    frame_len = int(w * h)
    f = open(filepath, 'rb')
    try:
        f.seek(frame_len * 4)
        raw = f.read(frame_len)
        u = np.frombuffer(raw, dtype=np.uint8)
        # cv2 grayscale image shape is height x width
        img_u = u.reshape(h, w)

    except Exception as e:
        print(str(e))
        return None
    return img_u


def load_V_From_YUV_I420(filepath):
    # yuv420 포맷에서 중 V 만 읽는다.
    img_name = os.path.basename(filepath)
    img_name = os.path.splitext(img_name)[0]
    w, h = img_name.split('_')[1].split('x')
    w, h = int(float(w)/2), int(float(h)/2)
    frame_len = int(w * h)
    f = open(filepath, 'rb')
    try:
        f.seek(frame_len * 5)
        raw = f.read(frame_len)
        v = np.frombuffer(raw, dtype=np.uint8)
        # cv2 grayscale image shape is height x width
        img_v = v.reshape(h, w)

    except Exception as e:
        print(str(e))
        return None
    return img_v


if __name__ == '__main__':
    y = load_U_From_YUV_I420('000000000009_640x480_P420_8b.yuv')
    print(y.dtype)
    cv2.imshow('image_', y)
    cv2.waitKey(0)


