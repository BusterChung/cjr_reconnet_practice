from os import listdir
from os.path import join

import torch.utils.data as data
from .dataIO import load_img
from .transforms import *


class PairedImageDataSet(data.Dataset):
    """
    https://www.youtube.com/watch?v=zN49HdDxHi8 참고
    """
    def __init__(self, origin_dir, artifact_dir, transform=None):
        self.artifact_filenames = [join(artifact_dir, x) for x in sorted(listdir(artifact_dir))]
        self.origin_filenames = [join(origin_dir, x) for x in sorted(listdir(origin_dir))]

        self.transform = transform

    def __getitem__(self, index):
        input = load_img(self.artifact_filenames[index])
        target = load_img(self.origin_filenames[index])
        if self.transform:
            input, target = self.transform(input, target)
        return input, target

    def __len__(self):
        return len(self.origin_filenames)


def folder_to_batch(folder_dir, transform=None):
    """
    :param folder_dir: batch 로 묶을 이미지들이 들어있는 폴더의 경로
    :param transform: RandomCrop, Color0_255to1_1, ToTensor 등
    :return: 폴더 안의 이미지들을 transform 으로 변환한 후 하나의 batch 로 묶은 것.
    """
    batch_paths = [join(folder_dir, x) for x in sorted(listdir(folder_dir))]
    if transform is None:
        batch = [load_img(path) for path in batch_paths]
    else:
        batch = [transform(load_img(path)) for path in batch_paths]

    # 이미지를 batch 로 묶을 때 numpy.ndarray 이미지가 transform 함수를 통해 torch.Tensor 가 됐다면
    # torch.stack 사용해야하고, 그대로 numpy.ndarray 라면 np.stack 사용해야 한다.
    if type(batch[0]) == torch.Tensor:
        batch = torch.stack(batch)
    else:
        batch = np.stack(batch)

    return batch

