from pathlib import Path
import time
import argparse

from pydooly import *
from models import *

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn
import torchvision  # torchvision 에서 제공 해주는 vgg19모델을 사용하기 위해 import 한다.

# cudnn.benchmark allows you to enable the inbuilt cudnn auto-tuner
# to find the best algorithm to use for your hardware.
# model 을 test 만 할 경우 False 로 해준는 것이 더 빠른 것 같다.
# training 할 때는 True 로 해주는 것이 더 빠르다.
cudnn.benchmark = True


def main(args):
    print('torch.__version__', torch.__version__)
    # 초기화 >>=========================================================================================================
    num_epochs = 1  # number of epochs to train for
    batch_size = 16  # training batch size
    learning_rate = 0.0001
    lr_decayed = 40

    sample_step = 2000
    input_channel = 1  # if color -> 3, if gray scale -> 1
    training_crop_size = 48  # 항상 crop size 는 짝수여야 한다.(one to many 의 구조 때문)
    testing_crop_size = 48 * 3  # 마찬가지로 짝수
    threads = 4  # number of threads for data loader to use
    device = torch.device('cuda:0')
    exp_name = args.exp_name  # 진행하는 실험의 경로. 이 경로에 모든 생성값들이 저장 된다.

    # training data set
    origin_dir = args.origin_dir
    artifact_dir = args.artifact_dir

    # test data set
    origin_test_dir = args.origin_test_dir
    artifact_test_dir = args.artifact_test_dir

    weight_save = False
    weight_load = False
    sample_save = False
    log_save = False

    # 학습 진행사항 저장 경로 >>=========================================================================================
    parent_dir = os.path.dirname(os.getcwd())

    # 모델의 weight 를 저장할 dir
    if weight_save or weight_load:
        dir_saved_net = make_dirs(f'{parent_dir}/exp/{exp_name}/saved_model') + '/' + '22v.pkl'

    # 중간중간 test 한 결과 이미지가 저장될 dir
    dir_sample = make_dirs(f'{parent_dir}/exp/{exp_name}/samples/')

    # 중간중간 log 값(loss, psnr 등) 이 저장될 파일의 dir
    if log_save:
        dir_log = make_dirs(f'{parent_dir}/exp/{exp_name}')
        args2memo(args, dir_log + "/exp_memo.txt")

    # ==================================================================================================================
    if not torch.cuda.is_available():
        raise Exception("No GPU found")
    else:
        print("===> GPU on")

    # training data set 과 test data set 생성 >>========================================================================
    print('===> Loading datasets')

    # Loading training data sets
    train_set = PairedImageDataSet(origin_dir, artifact_dir,
                                   Compose([
                                       RandomCrop(training_crop_size),
                                       Color0_255to1_1(),  # [0,255] 범위의 이미지를 [-1, 1] 사이로 변환해준다.
                                       ToTensor()
                                   ])
                                   )

    training_data_loader = DataLoader(dataset=train_set,
                                      num_workers=threads,
                                      batch_size=batch_size,
                                      shuffle=True,
                                      drop_last=True,
                                      )

    # Loading test data sets
    test_batch_origin = folder_to_batch(origin_test_dir,
                                        Compose([
                                            CenterCrop(testing_crop_size),
                                            Color0_255to1_1(),
                                            ToTensor()
                                        ])
                                        ).to(device)
    test_batch_artifact = folder_to_batch(artifact_test_dir,
                                          Compose([
                                              CenterCrop(testing_crop_size),
                                              Color0_255to1_1(),
                                              ToTensor()
                                          ])
                                          ).to(device)

    # inverse_transform 을 통해 <class 'torch.Tensor'> 를 <class 'numpy.ndarray'> 로 변환할 수 있다.
    inverse_transform = Compose([
        ToImage(),
        Color1_1to0_255(),  # [-1, 1] 범위의 이미지를 [0,255] 사이로 변환해준다.
        MergeNP((3, 3)),  # 이미지들을 3x3 의 타일 형태의 하나의 이미지로 만들어준다.
    ])

    # 모델 생성 >>=======================================================================================================
    print('===> Building model')
    # Generator 생성, 초기화
    netG = Generator_one2many_RDB(input_channel).to(device)
    netG.apply(weights_init)

    print('===> Number of params: {}'.format(
        sum([p.data.nelement() for p in netG.parameters()])))

    # Discriminator, Generator 의 optimizer 생성
    G_optimizer = torch.optim.Adam(netG.parameters(), lr=learning_rate, betas=(0.5, 0.999))

    # criterion 생성 >>=================================================================================================
    MSE = nn.MSELoss().to(device)

    # 저장된 checkpoint 가 있다면 불러오기!! >>===========================================================================
    start_epoch = 1
    best_psnr = 0

    # optionally resume from a checkpoint
    if weight_load:
        if os.path.isfile(dir_saved_net):
            print("===> loading checkpoint '{}'".format(dir_saved_net))

            checkpoint = torch.load(dir_saved_net)

            start_epoch = checkpoint['epoch']
            best_psnr = checkpoint['best_psnr']

            netG.load_state_dict(checkpoint['state_dict_G'])
            G_optimizer.load_state_dict(checkpoint['optimizer_G'])
        else:
            print("===> no checkpoint found at '{}'".format(dir_saved_net))

    if log_save:
        # log 의 header 를 만들어준다.
        logging = LogCSV(log_dir=dir_log + "/log.csv",
                         header=['epoch', 'psnr(ori, art)', 'psnr(ori, recon)', 'best_psnr'],
                         epoch=start_epoch)

    # ==================================================================================================================
    # Generator 을 test 할 때 사용.
    def test():
        # torch.no_grad() 를 통해 test 시 미분 등을 하지 않는다.
        with torch.no_grad():
            """
            eval() 을 통해 batch norm 이나 drop out (training 때 와 testing 할 때 계산이 다른 함수들) 등
            에게 test 중이라고 알린다.
            """
            netG.eval()
            prediction = netG(test_batch_artifact)
            return prediction

    # Generator 만을 MSE 로  학습 시키는 경우.
    def train_base(epoch):
        for i, batch in enumerate(training_data_loader, 1):
            netG.train()
            img_input, img_target = batch[0].to(device), batch[1].to(device)
            output = netG(img_input)
            loss = MSE(output, img_target)

            G_optimizer.zero_grad()
            loss.backward()
            G_optimizer.step()

            if i % 20 == 0:
                print(f"===> Epoch[{epoch}/{num_epochs}]({i}/{len(training_data_loader)}): Loss: {loss.item():.6f}")

            # save the sampled images
            if sample_save:
                if (i) % sample_step == 0:
                    prediction = test()
                    prediction = inverse_transform(prediction)
                    sample_name = '%03d_%04d.png' % (epoch, i)
                    cv2.imwrite(filename=dir_sample + '/' + sample_name, img=prediction.astype(np.uint8))

    # 학습 시작 >>=======================================================================================================
    print('===> Start training')
    # test set 의 원본 이미지를 타일 형태로 저장하여 복원된 이미지와 비교할 수 있게 한다.
    ori_img = inverse_transform(test_batch_origin)
    cv2.imwrite(filename=dir_sample + '/' + 'ori.png', img=ori_img.astype(np.uint8))

    # test set 의 훼손 이미지를 타일 형태로 저장하여 복원된 이미지와 비교할 수 있게 한다.
    art_img = inverse_transform(test_batch_artifact)
    cv2.imwrite(filename=dir_sample + '/' + 'art.png', img=art_img.astype(np.uint8))

    # 훼손된 이미지와 원본 이미지와의 psnr 을 구해본다.
    psnr_art_ori = get_psnr(test_batch_artifact, test_batch_origin, -1, 1)
    print(f"Avg. PSNR(artifact, origin): {psnr_art_ori:.4f} dB")

    # 초기 모델을 통해 복구한 이미지와 원본 이미지와의 psnr 을 구해본다.
    psnr_recon_ori = get_psnr(test(), test_batch_origin, -1, 1)
    print(f"Avg. PSNR(recon, origin): {psnr_recon_ori:.4f} dB")

    start_time = time.time()
    for epoch in range(start_epoch, num_epochs+1):
        print(f'epoch {epoch} start -------------------------------------------------------------')

        adjust_learning_rate(init_lr=learning_rate, optimizer=G_optimizer, epoch=epoch, n=lr_decayed)

        train_base(epoch)

        # 한 epoch 학습 후 test set 을 이용한 model 평가
        psnr_recon_ori = get_psnr(test(), test_batch_origin, -1, 1)
        print(f"epoch : {epoch} Avg. PSNR(recon, origin): {psnr_recon_ori:.4f} dB (Best : {best_psnr:.4f} dB)")

        # Save log
        if log_save:
            logging([epoch, psnr_art_ori, psnr_recon_ori, best_psnr])

        # check point 를 저장한다.
        if weight_save:
            # best psnr 을 확인하여 best 인 모델은 따로 저장하게 한다.
            is_best = psnr_recon_ori > best_psnr
            best_psnr = max(psnr_recon_ori, best_psnr)

            save_checkpoint(
                {
                'epoch': epoch,
                'best_psnr': best_psnr,
                'state_dict_G': netG.state_dict(),
                'optimizer_G': G_optimizer.state_dict(),
                },
                is_best,
                filename=dir_saved_net)

        # Time spent
        end_time = time.time()
        print(f"Time spent : {seconds_to_hours(end_time - start_time)}")


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='parser for image recon net')

    parser.add_argument('--origin_dir', type=str,
                        default='/home/datasets/coco2017/yuv_video_I420/ori')
    parser.add_argument('--artifact_dir', type=str,
                        default='/home/datasets/coco2017/yuv_video_I420/qp22')
    parser.add_argument('--origin_test_dir', type=str,
                        default='/home/datasets/testdata/hevc/ori')
    parser.add_argument('--artifact_test_dir', type=str,
                        default='/home/datasets/testdata/hevc/qp22')

    parser.add_argument('--exp_name', type=str, default='exp001')
    parser.add_argument('--exp_memo', type=str,
                        default='reconNet_2.0 만들고 처음 만들어보는 결과. 코드 테스트 용이다.')

    args = parser.parse_args()

    main(args)
