"""
복원된 yuv_I420_video 의 psnr 을 구한다.
"""

from pydooly import *
from os.path import join
import tqdm


def ctc_of_hevc_psnr(ori_dir, recon_dir, log_dir):
    """
    ctc_of_hevc의 모든 비디오에 대해 복원된 비디오와 원본 비디오 사이의 psnr 을 자동을로 구해준다.
    최종 결과는 엑셀 파일이다.
    """
    ori = [join(ori_dir, x) for x in listdir(ori_dir)]
    logging = LogCSV2(log_dir=log_dir, header=['Condition', 'Sequence', 'y', 'u', 'v'])

    folder1s = [join(recon_dir, x) for x in listdir(recon_dir)]
    print(folder1s)

    for folder1 in folder1s:
        folder2s = [join(folder1, x) for x in listdir(folder1)]
        for folder2 in folder2s:
            recon_videos = [join(folder2, x) for x in listdir(folder2)]
            for recon_video in tqdm.tqdm(recon_videos):
                # 비디오의 압축 방식을 읽는다. (AI or RA or LB or LP)
                condition = folder1.split(']')[1]

                # 복원할 비디오의 번호를 읽는다. (S01 ~ S17)
                sequence_num = int(os.path.basename(recon_video).split('_')[0].split('S')[1]) - 1

                # 위에서 읽은 복원할 비디오의 번호에 맞는 원본 이미지를 읽는다.
                video_dir_ori = ori[sequence_num]

                # 복원할 비디오의 크기 (height, width)를 읽는다.
                size = os.path.basename(recon_video).split('_')[2].split('x')
                size = (int(size[1]), int(size[0]))

                # 복원된 비디오와 원본 비디오의 psnr 을 구한다.
                recon_psnr_y, recon_psnr_u, recon_psnr_v = get_psnr_for_video(video_dir_ori, recon_video, size)

                # 구한 psnr을 엑셀에 입력한다.
                logging([condition, os.path.basename(recon_video), recon_psnr_y, recon_psnr_u, recon_psnr_v])


if __name__ == '__main__':
    # # 원본 파일들의 폴더 dir
    # ori_dir = r'D:\Data\ctc_of_hevc\[00]sequence'
    #
    # # 복원할 비디오들의 폴더 dir
    # recon_dir = r'D:\kdw\reconNet_1.1\eval2'
    #
    # # 결과파일을 저장할 엑셀 파일의 dir
    # log_dir = r'D:\kdw\reconNet_1.1\results11.csv'
    #
    # # psnr 구하기!
    # ctc_of_hevc_psnr(ori_dir, recon_dir, log_dir)

    video_dir_ori_07_LP = r'D:\Data\ctc_of_hevc\[00]sequence\S07_Cactus_1920x1080_P420_50Hz_8b_500frm.yuv'
    video_dir_ori_09_LP = r'D:\Data\ctc_of_hevc\[00]sequence\S09_BQTerrace_1920x1080__P420_60Hz_8b_600frm.yuv'
    video_dir_ori_15_LP = r'D:\Data\ctc_of_hevc\[00]sequence\S15_BQSquare_416x240_P420_60Hz.yuv'

    recon_video_07_LP = r'D:\kdw\reconNet_1.1\eval2\[4]LowDelay-P\[1]ClassB\S07_Cactus_1920x1080_P420_50Hz_8b_500frm_QP22_LDB_recon.yuv'
    recon_video_09_LP = r'D:\kdw\reconNet_1.1\eval2\[4]LowDelay-P\[1]ClassB\S09_BQTerrace_1920x1080__P420_60Hz_8b_600frm_QP22_LDB_recon.yuv'
    recon_video_15_LP = r'D:\kdw\reconNet_1.1\eval2\[4]LowDelay-P\[3]ClassD\S15_BQSquare_416x240_P420_60Hz_600frm_QP22_LDB_recon.yuv'

    log_dir = r'D:\kdw\reconNet_1.1\frame_log.csv'

    # (height, width)
    size = (240, 416)
    recon_psnr_y, recon_psnr_u, recon_psnr_v = get_psnr_for_video(video_dir_ori_15_LP, recon_video_15_LP, size, log_dir=log_dir)
    print(recon_psnr_y, recon_psnr_u, recon_psnr_v)
